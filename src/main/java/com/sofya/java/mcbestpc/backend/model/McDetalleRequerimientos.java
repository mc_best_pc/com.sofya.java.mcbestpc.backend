/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_DETALLE_REQUERIMIENTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McDetalleRequerimientos.findAll", query = "SELECT m FROM McDetalleRequerimientos m")
    , @NamedQuery(name = "McDetalleRequerimientos.findByDetalleRequerimientoId", query = "SELECT m FROM McDetalleRequerimientos m WHERE m.detalleRequerimientoId = :detalleRequerimientoId")
    , @NamedQuery(name = "McDetalleRequerimientos.findByItems", query = "SELECT m FROM McDetalleRequerimientos m WHERE m.items = :items")
    , @NamedQuery(name = "McDetalleRequerimientos.findByCantidad", query = "SELECT m FROM McDetalleRequerimientos m WHERE m.cantidad = :cantidad")
    , @NamedQuery(name = "McDetalleRequerimientos.findByDescripcion", query = "SELECT m FROM McDetalleRequerimientos m WHERE m.descripcion = :descripcion")
    , @NamedQuery(name = "McDetalleRequerimientos.findByEstado", query = "SELECT m FROM McDetalleRequerimientos m WHERE m.estado = :estado")})
public class McDetalleRequerimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DETALLE_REQUERIMIENTO_ID")
    private Integer detalleRequerimientoId;
    @Size(max = 128)
    @Column(name = "ITEMS")
    private String items;
    @Column(name = "CANTIDAD")
    private Integer cantidad;
    @Size(max = 264)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "MC_REQUERIMIENTO_ID", referencedColumnName = "REQUERIMIENTO_ID")
    @ManyToOne
    private McRequerimiento mcRequerimientoId;

    public McDetalleRequerimientos() {
    }

    public McDetalleRequerimientos(Integer detalleRequerimientoId) {
        this.detalleRequerimientoId = detalleRequerimientoId;
    }

    public Integer getDetalleRequerimientoId() {
        return detalleRequerimientoId;
    }

    public void setDetalleRequerimientoId(Integer detalleRequerimientoId) {
        this.detalleRequerimientoId = detalleRequerimientoId;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public McRequerimiento getMcRequerimientoId() {
        return mcRequerimientoId;
    }

    public void setMcRequerimientoId(McRequerimiento mcRequerimientoId) {
        this.mcRequerimientoId = mcRequerimientoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleRequerimientoId != null ? detalleRequerimientoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McDetalleRequerimientos)) {
            return false;
        }
        McDetalleRequerimientos other = (McDetalleRequerimientos) object;
        if ((this.detalleRequerimientoId == null && other.detalleRequerimientoId != null) || (this.detalleRequerimientoId != null && !this.detalleRequerimientoId.equals(other.detalleRequerimientoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McDetalleRequerimientos[ detalleRequerimientoId=" + detalleRequerimientoId + " ]";
    }
    
}
