package com.sofya.java.mcbestpc.backend.constant;

public enum EstadoEnum {
    ACTIVO((String) "S"),
    INACTIVO((String) "N");

    private String estado;

    private EstadoEnum(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public static EstadoEnum obtenerEstadoPorCodigo(String estado) {
        EstadoEnum valor = null;
        for (EstadoEnum e : EstadoEnum.values()) {
            if (e.getEstado() == estado) {
                valor = e;
                break;
            }
        }
        return valor;
    }
}
