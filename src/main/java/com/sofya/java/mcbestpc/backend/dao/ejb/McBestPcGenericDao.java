package com.sofya.java.mcbestpc.backend.dao.ejb;

import com.sofya.java.uphone.backend.persistence.dao.ejb.GenericEmDaoEjb;
import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class McBestPcGenericDao<T, PK extends Serializable> extends GenericEmDaoEjb<T, PK> {

    public McBestPcGenericDao(Class<T> type) {
        super(type);
    }

    @PersistenceContext(unitName = "MC_BEST_PC")
    protected EntityManager em;

    @Override
    protected EntityManager getEm() {
        return this.em;
    }
}
