/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_CONSOLIDADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McConsolidado.findAll", query = "SELECT m FROM McConsolidado m")
    , @NamedQuery(name = "McConsolidado.findByConsolidadoId", query = "SELECT m FROM McConsolidado m WHERE m.consolidadoId = :consolidadoId")
    , @NamedQuery(name = "McConsolidado.findByFechaConsolidado", query = "SELECT m FROM McConsolidado m WHERE m.fechaConsolidado = :fechaConsolidado")
    , @NamedQuery(name = "McConsolidado.findByEstado", query = "SELECT m FROM McConsolidado m WHERE m.estado = :estado")})
public class McConsolidado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSOLIDADO_ID")
    private Integer consolidadoId;
    @Column(name = "FECHA_CONSOLIDADO")
    @Temporal(TemporalType.DATE)
    private Date fechaConsolidado;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "mcConsolidadoId")
    private List<McDetalleConsolidado> mcDetalleConsolidadoList;
    @OneToMany(mappedBy = "mcConsolidadoId")
    private List<McRequerimientoConsolidado> mcRequerimientoConsolidadoList;

    public McConsolidado() {
    }

    public McConsolidado(Integer consolidadoId) {
        this.consolidadoId = consolidadoId;
    }

    public Integer getConsolidadoId() {
        return consolidadoId;
    }

    public void setConsolidadoId(Integer consolidadoId) {
        this.consolidadoId = consolidadoId;
    }

    public Date getFechaConsolidado() {
        return fechaConsolidado;
    }

    public void setFechaConsolidado(Date fechaConsolidado) {
        this.fechaConsolidado = fechaConsolidado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<McDetalleConsolidado> getMcDetalleConsolidadoList() {
        return mcDetalleConsolidadoList;
    }

    public void setMcDetalleConsolidadoList(List<McDetalleConsolidado> mcDetalleConsolidadoList) {
        this.mcDetalleConsolidadoList = mcDetalleConsolidadoList;
    }

    @XmlTransient
    public List<McRequerimientoConsolidado> getMcRequerimientoConsolidadoList() {
        return mcRequerimientoConsolidadoList;
    }

    public void setMcRequerimientoConsolidadoList(List<McRequerimientoConsolidado> mcRequerimientoConsolidadoList) {
        this.mcRequerimientoConsolidadoList = mcRequerimientoConsolidadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (consolidadoId != null ? consolidadoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McConsolidado)) {
            return false;
        }
        McConsolidado other = (McConsolidado) object;
        if ((this.consolidadoId == null && other.consolidadoId != null) || (this.consolidadoId != null && !this.consolidadoId.equals(other.consolidadoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McConsolidado[ consolidadoId=" + consolidadoId + " ]";
    }
    
}
