/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_PROVEEDOR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McProveedor.findAll", query = "SELECT m FROM McProveedor m")
    , @NamedQuery(name = "McProveedor.findByProveedorId", query = "SELECT m FROM McProveedor m WHERE m.proveedorId = :proveedorId")
    , @NamedQuery(name = "McProveedor.findByEstado", query = "SELECT m FROM McProveedor m WHERE m.estado = :estado")})
public class McProveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PROVEEDOR_ID")
    private Integer proveedorId;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "proveedorId")
    private List<McCotizacion> mcCotizacionList;

    public McProveedor() {
    }

    public McProveedor(Integer proveedorId) {
        this.proveedorId = proveedorId;
    }

    public Integer getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(Integer proveedorId) {
        this.proveedorId = proveedorId;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<McCotizacion> getMcCotizacionList() {
        return mcCotizacionList;
    }

    public void setMcCotizacionList(List<McCotizacion> mcCotizacionList) {
        this.mcCotizacionList = mcCotizacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proveedorId != null ? proveedorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McProveedor)) {
            return false;
        }
        McProveedor other = (McProveedor) object;
        if ((this.proveedorId == null && other.proveedorId != null) || (this.proveedorId != null && !this.proveedorId.equals(other.proveedorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McProveedor[ proveedorId=" + proveedorId + " ]";
    }
    
}
