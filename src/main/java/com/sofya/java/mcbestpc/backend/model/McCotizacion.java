/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_COTIZACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McCotizacion.findAll", query = "SELECT m FROM McCotizacion m")
    , @NamedQuery(name = "McCotizacion.findByIdCotizacion", query = "SELECT m FROM McCotizacion m WHERE m.idCotizacion = :idCotizacion")
    , @NamedQuery(name = "McCotizacion.findByFecha", query = "SELECT m FROM McCotizacion m WHERE m.fecha = :fecha")
    , @NamedQuery(name = "McCotizacion.findByEstado", query = "SELECT m FROM McCotizacion m WHERE m.estado = :estado")})
public class McCotizacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_COTIZACION")
    private Integer idCotizacion;
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "idCotizacion")
    private List<McDetalleCotizacion> mcDetalleCotizacionList;
    @JoinColumn(name = "PROVEEDOR_ID", referencedColumnName = "PROVEEDOR_ID")
    @ManyToOne
    private McProveedor proveedorId;
    @OneToMany(mappedBy = "idCotizacion")
    private List<McReqConsCotizacion> mcReqConsCotizacionList;

    public McCotizacion() {
    }

    public McCotizacion(Integer idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public Integer getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(Integer idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<McDetalleCotizacion> getMcDetalleCotizacionList() {
        return mcDetalleCotizacionList;
    }

    public void setMcDetalleCotizacionList(List<McDetalleCotizacion> mcDetalleCotizacionList) {
        this.mcDetalleCotizacionList = mcDetalleCotizacionList;
    }

    public McProveedor getProveedorId() {
        return proveedorId;
    }

    public void setProveedorId(McProveedor proveedorId) {
        this.proveedorId = proveedorId;
    }

    @XmlTransient
    public List<McReqConsCotizacion> getMcReqConsCotizacionList() {
        return mcReqConsCotizacionList;
    }

    public void setMcReqConsCotizacionList(List<McReqConsCotizacion> mcReqConsCotizacionList) {
        this.mcReqConsCotizacionList = mcReqConsCotizacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCotizacion != null ? idCotizacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McCotizacion)) {
            return false;
        }
        McCotizacion other = (McCotizacion) object;
        if ((this.idCotizacion == null && other.idCotizacion != null) || (this.idCotizacion != null && !this.idCotizacion.equals(other.idCotizacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McCotizacion[ idCotizacion=" + idCotizacion + " ]";
    }
    
}
