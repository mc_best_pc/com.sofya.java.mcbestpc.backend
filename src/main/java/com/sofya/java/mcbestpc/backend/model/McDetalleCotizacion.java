/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_DETALLE_COTIZACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McDetalleCotizacion.findAll", query = "SELECT m FROM McDetalleCotizacion m")
    , @NamedQuery(name = "McDetalleCotizacion.findByDetalleCotizacionId", query = "SELECT m FROM McDetalleCotizacion m WHERE m.detalleCotizacionId = :detalleCotizacionId")
    , @NamedQuery(name = "McDetalleCotizacion.findByCantidad", query = "SELECT m FROM McDetalleCotizacion m WHERE m.cantidad = :cantidad")
    , @NamedQuery(name = "McDetalleCotizacion.findByEstado", query = "SELECT m FROM McDetalleCotizacion m WHERE m.estado = :estado")})
public class McDetalleCotizacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DETALLE_COTIZACION_ID")
    private Integer detalleCotizacionId;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ITEMS")
    private String items;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "CANTIDAD")
    private Integer cantidad;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "ID_COTIZACION", referencedColumnName = "ID_COTIZACION")
    @ManyToOne
    private McCotizacion idCotizacion;

    public McDetalleCotizacion() {
    }

    public McDetalleCotizacion(Integer detalleCotizacionId) {
        this.detalleCotizacionId = detalleCotizacionId;
    }

    public Integer getDetalleCotizacionId() {
        return detalleCotizacionId;
    }

    public void setDetalleCotizacionId(Integer detalleCotizacionId) {
        this.detalleCotizacionId = detalleCotizacionId;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public McCotizacion getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(McCotizacion idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleCotizacionId != null ? detalleCotizacionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McDetalleCotizacion)) {
            return false;
        }
        McDetalleCotizacion other = (McDetalleCotizacion) object;
        if ((this.detalleCotizacionId == null && other.detalleCotizacionId != null) || (this.detalleCotizacionId != null && !this.detalleCotizacionId.equals(other.detalleCotizacionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McDetalleCotizacion[ detalleCotizacionId=" + detalleCotizacionId + " ]";
    }
    
}
