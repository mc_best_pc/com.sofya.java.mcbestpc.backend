/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_REQ_CONS_COTIZACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McReqConsCotizacion.findAll", query = "SELECT m FROM McReqConsCotizacion m")
    , @NamedQuery(name = "McReqConsCotizacion.findByReqConsCotizacion", query = "SELECT m FROM McReqConsCotizacion m WHERE m.reqConsCotizacion = :reqConsCotizacion")
    , @NamedQuery(name = "McReqConsCotizacion.findByFecha", query = "SELECT m FROM McReqConsCotizacion m WHERE m.fecha = :fecha")
    , @NamedQuery(name = "McReqConsCotizacion.findByEstado", query = "SELECT m FROM McReqConsCotizacion m WHERE m.estado = :estado")})
public class McReqConsCotizacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "REQ_CONS_COTIZACION")
    private Integer reqConsCotizacion;
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "ID_COTIZACION", referencedColumnName = "ID_COTIZACION")
    @ManyToOne
    private McCotizacion idCotizacion;
    @JoinColumn(name = "REQUERIMIENTO_CONSOLIDADO_ID", referencedColumnName = "REQUERIMIENTO_CONSOLIDADO_ID")
    @ManyToOne
    private McRequerimientoConsolidado requerimientoConsolidadoId;

    public McReqConsCotizacion() {
    }

    public McReqConsCotizacion(Integer reqConsCotizacion) {
        this.reqConsCotizacion = reqConsCotizacion;
    }

    public Integer getReqConsCotizacion() {
        return reqConsCotizacion;
    }

    public void setReqConsCotizacion(Integer reqConsCotizacion) {
        this.reqConsCotizacion = reqConsCotizacion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public McCotizacion getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(McCotizacion idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public McRequerimientoConsolidado getRequerimientoConsolidadoId() {
        return requerimientoConsolidadoId;
    }

    public void setRequerimientoConsolidadoId(McRequerimientoConsolidado requerimientoConsolidadoId) {
        this.requerimientoConsolidadoId = requerimientoConsolidadoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reqConsCotizacion != null ? reqConsCotizacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McReqConsCotizacion)) {
            return false;
        }
        McReqConsCotizacion other = (McReqConsCotizacion) object;
        if ((this.reqConsCotizacion == null && other.reqConsCotizacion != null) || (this.reqConsCotizacion != null && !this.reqConsCotizacion.equals(other.reqConsCotizacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McReqConsCotizacion[ reqConsCotizacion=" + reqConsCotizacion + " ]";
    }
    
}
