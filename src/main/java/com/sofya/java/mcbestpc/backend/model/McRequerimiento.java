/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_REQUERIMIENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McRequerimiento.findAll", query = "SELECT m FROM McRequerimiento m")
    , @NamedQuery(name = "McRequerimiento.findByRequerimientoId", query = "SELECT m FROM McRequerimiento m WHERE m.requerimientoId = :requerimientoId")
    , @NamedQuery(name = "McRequerimiento.findByFecha", query = "SELECT m FROM McRequerimiento m WHERE m.fecha = :fecha")
    , @NamedQuery(name = "McRequerimiento.findByEstado", query = "SELECT m FROM McRequerimiento m WHERE m.estado = :estado")})
public class McRequerimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "REQUERIMIENTO_ID")
    private Integer requerimientoId;
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "mcRequerimientoId")
    private List<McDetalleRequerimientos> mcDetalleRequerimientosList;
    @JoinColumn(name = "MC_AGENCIA_ID", referencedColumnName = "AGENCIA_ID")
    @ManyToOne
    private McAgencia mcAgenciaId;
    @OneToMany(mappedBy = "mcRequerimientoId")
    private List<McRequerimientoConsolidado> mcRequerimientoConsolidadoList;

    public McRequerimiento() {
    }

    public McRequerimiento(Integer requerimientoId) {
        this.requerimientoId = requerimientoId;
    }

    public Integer getRequerimientoId() {
        return requerimientoId;
    }

    public void setRequerimientoId(Integer requerimientoId) {
        this.requerimientoId = requerimientoId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<McDetalleRequerimientos> getMcDetalleRequerimientosList() {
        return mcDetalleRequerimientosList;
    }

    public void setMcDetalleRequerimientosList(List<McDetalleRequerimientos> mcDetalleRequerimientosList) {
        this.mcDetalleRequerimientosList = mcDetalleRequerimientosList;
    }

    public McAgencia getMcAgenciaId() {
        return mcAgenciaId;
    }

    public void setMcAgenciaId(McAgencia mcAgenciaId) {
        this.mcAgenciaId = mcAgenciaId;
    }

    @XmlTransient
    public List<McRequerimientoConsolidado> getMcRequerimientoConsolidadoList() {
        return mcRequerimientoConsolidadoList;
    }

    public void setMcRequerimientoConsolidadoList(List<McRequerimientoConsolidado> mcRequerimientoConsolidadoList) {
        this.mcRequerimientoConsolidadoList = mcRequerimientoConsolidadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requerimientoId != null ? requerimientoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McRequerimiento)) {
            return false;
        }
        McRequerimiento other = (McRequerimiento) object;
        if ((this.requerimientoId == null && other.requerimientoId != null) || (this.requerimientoId != null && !this.requerimientoId.equals(other.requerimientoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McRequerimiento[ requerimientoId=" + requerimientoId + " ]";
    }
    
}
