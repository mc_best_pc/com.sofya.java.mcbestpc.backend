/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.dao;

import com.sofya.java.uphone.backend.persistence.dao.GenericDao;
import com.sofya.java.mcbestpc.backend.model.McAgencia;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Roger Pilacuan
 */
@Local
public interface AgenciaDao extends GenericDao<McAgencia, Integer> {

    /*
    Método: public List<McAgencia> getListAgency();
    Descripción: Lista todas las agencias
     */
    public List<McAgencia> getListAgency();

    /*
    Método: public String deleteDepos(int bodegaId);
    Descripción: Elimina de forma lógica la bodega por su id
     */
//    public String deleteDepos(int bodegaId);
}
