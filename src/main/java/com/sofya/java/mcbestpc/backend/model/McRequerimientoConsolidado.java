/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_REQUERIMIENTO_CONSOLIDADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McRequerimientoConsolidado.findAll", query = "SELECT m FROM McRequerimientoConsolidado m")
    , @NamedQuery(name = "McRequerimientoConsolidado.findByRequerimientoConsolidadoId", query = "SELECT m FROM McRequerimientoConsolidado m WHERE m.requerimientoConsolidadoId = :requerimientoConsolidadoId")
    , @NamedQuery(name = "McRequerimientoConsolidado.findByFechaAprobacion", query = "SELECT m FROM McRequerimientoConsolidado m WHERE m.fechaAprobacion = :fechaAprobacion")
    , @NamedQuery(name = "McRequerimientoConsolidado.findByUsuario", query = "SELECT m FROM McRequerimientoConsolidado m WHERE m.usuario = :usuario")
    , @NamedQuery(name = "McRequerimientoConsolidado.findByEstado", query = "SELECT m FROM McRequerimientoConsolidado m WHERE m.estado = :estado")})
public class McRequerimientoConsolidado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "REQUERIMIENTO_CONSOLIDADO_ID")
    private Integer requerimientoConsolidadoId;
    @Column(name = "FECHA_APROBACION")
    @Temporal(TemporalType.DATE)
    private Date fechaAprobacion;
    @Size(max = 12)
    @Column(name = "USUARIO")
    private String usuario;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "MC_CONSOLIDADO_ID", referencedColumnName = "CONSOLIDADO_ID")
    @ManyToOne
    private McConsolidado mcConsolidadoId;
    @JoinColumn(name = "MC_REQUERIMIENTO_ID", referencedColumnName = "REQUERIMIENTO_ID")
    @ManyToOne
    private McRequerimiento mcRequerimientoId;
    @OneToMany(mappedBy = "requerimientoConsolidadoId")
    private List<McReqConsCotizacion> mcReqConsCotizacionList;

    public McRequerimientoConsolidado() {
    }

    public McRequerimientoConsolidado(Integer requerimientoConsolidadoId) {
        this.requerimientoConsolidadoId = requerimientoConsolidadoId;
    }

    public Integer getRequerimientoConsolidadoId() {
        return requerimientoConsolidadoId;
    }

    public void setRequerimientoConsolidadoId(Integer requerimientoConsolidadoId) {
        this.requerimientoConsolidadoId = requerimientoConsolidadoId;
    }

    public Date getFechaAprobacion() {
        return fechaAprobacion;
    }

    public void setFechaAprobacion(Date fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public McConsolidado getMcConsolidadoId() {
        return mcConsolidadoId;
    }

    public void setMcConsolidadoId(McConsolidado mcConsolidadoId) {
        this.mcConsolidadoId = mcConsolidadoId;
    }

    public McRequerimiento getMcRequerimientoId() {
        return mcRequerimientoId;
    }

    public void setMcRequerimientoId(McRequerimiento mcRequerimientoId) {
        this.mcRequerimientoId = mcRequerimientoId;
    }

    @XmlTransient
    public List<McReqConsCotizacion> getMcReqConsCotizacionList() {
        return mcReqConsCotizacionList;
    }

    public void setMcReqConsCotizacionList(List<McReqConsCotizacion> mcReqConsCotizacionList) {
        this.mcReqConsCotizacionList = mcReqConsCotizacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requerimientoConsolidadoId != null ? requerimientoConsolidadoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McRequerimientoConsolidado)) {
            return false;
        }
        McRequerimientoConsolidado other = (McRequerimientoConsolidado) object;
        if ((this.requerimientoConsolidadoId == null && other.requerimientoConsolidadoId != null) || (this.requerimientoConsolidadoId != null && !this.requerimientoConsolidadoId.equals(other.requerimientoConsolidadoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McRequerimientoConsolidado[ requerimientoConsolidadoId=" + requerimientoConsolidadoId + " ]";
    }
    
}
