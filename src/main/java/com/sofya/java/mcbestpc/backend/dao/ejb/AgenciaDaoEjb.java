/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.dao.ejb;

import com.sofya.java.mcbestpc.backend.constant.EstadoEnum;
import com.sofya.java.mcbestpc.backend.dao.AgenciaDao;
import com.sofya.java.mcbestpc.backend.model.McAgencia;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Roger Pilacuan
 */
@Stateless(name = "AgenciaDao")
public class AgenciaDaoEjb extends McBestPcGenericDao<McAgencia, Integer> implements AgenciaDao {

    public AgenciaDaoEjb() {
        super(McAgencia.class);
    }

    @Override
    public List<McAgencia> getListAgency() {
        Query query = em.createQuery("SELECT mca FROM McAgencia mca WHERE mca.estado=:estado");
        query.setParameter("estado", EstadoEnum.ACTIVO.getEstado());
        List<McAgencia> mcAgenciaList = new ArrayList<McAgencia>();
        mcAgenciaList = query.getResultList();
        return mcAgenciaList;
    }
}
