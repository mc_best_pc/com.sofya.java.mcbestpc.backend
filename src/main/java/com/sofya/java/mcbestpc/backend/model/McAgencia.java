/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_AGENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McAgencia.findAll", query = "SELECT m FROM McAgencia m")
    , @NamedQuery(name = "McAgencia.findByAgenciaId", query = "SELECT m FROM McAgencia m WHERE m.agenciaId = :agenciaId")
    , @NamedQuery(name = "McAgencia.findByNombre", query = "SELECT m FROM McAgencia m WHERE m.nombre = :nombre")
    , @NamedQuery(name = "McAgencia.findByDireccion", query = "SELECT m FROM McAgencia m WHERE m.direccion = :direccion")
    , @NamedQuery(name = "McAgencia.findByPuntoEmision", query = "SELECT m FROM McAgencia m WHERE m.puntoEmision = :puntoEmision")
    , @NamedQuery(name = "McAgencia.findByEstado", query = "SELECT m FROM McAgencia m WHERE m.estado = :estado")})
public class McAgencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AGENCIA_ID")
    private Integer agenciaId;
    @Size(max = 64)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 264)
    @Column(name = "DIRECCION")
    private String direccion;
    @Size(max = 13)
    @Column(name = "PUNTO_EMISION")
    private String puntoEmision;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "mcAgenciaId")
    private List<McRequerimiento> mcRequerimientoList;

    public McAgencia() {
    }

    public McAgencia(Integer agenciaId) {
        this.agenciaId = agenciaId;
    }

    public Integer getAgenciaId() {
        return agenciaId;
    }

    public void setAgenciaId(Integer agenciaId) {
        this.agenciaId = agenciaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPuntoEmision() {
        return puntoEmision;
    }

    public void setPuntoEmision(String puntoEmision) {
        this.puntoEmision = puntoEmision;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<McRequerimiento> getMcRequerimientoList() {
        return mcRequerimientoList;
    }

    public void setMcRequerimientoList(List<McRequerimiento> mcRequerimientoList) {
        this.mcRequerimientoList = mcRequerimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agenciaId != null ? agenciaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McAgencia)) {
            return false;
        }
        McAgencia other = (McAgencia) object;
        if ((this.agenciaId == null && other.agenciaId != null) || (this.agenciaId != null && !this.agenciaId.equals(other.agenciaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McAgencia[ agenciaId=" + agenciaId + " ]";
    }
    
}
