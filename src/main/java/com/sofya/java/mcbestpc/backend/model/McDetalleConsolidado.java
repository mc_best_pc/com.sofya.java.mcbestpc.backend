/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sofya.java.mcbestpc.backend.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Roger Pilacuan
 */
@Entity
@Table(name = "MC_DETALLE_CONSOLIDADO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McDetalleConsolidado.findAll", query = "SELECT m FROM McDetalleConsolidado m")
    , @NamedQuery(name = "McDetalleConsolidado.findByDetalleConsolidadoId", query = "SELECT m FROM McDetalleConsolidado m WHERE m.detalleConsolidadoId = :detalleConsolidadoId")
    , @NamedQuery(name = "McDetalleConsolidado.findByItems", query = "SELECT m FROM McDetalleConsolidado m WHERE m.items = :items")
    , @NamedQuery(name = "McDetalleConsolidado.findByCantidad", query = "SELECT m FROM McDetalleConsolidado m WHERE m.cantidad = :cantidad")
    , @NamedQuery(name = "McDetalleConsolidado.findByDescripcion", query = "SELECT m FROM McDetalleConsolidado m WHERE m.descripcion = :descripcion")
    , @NamedQuery(name = "McDetalleConsolidado.findByEstado", query = "SELECT m FROM McDetalleConsolidado m WHERE m.estado = :estado")})
public class McDetalleConsolidado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DETALLE_CONSOLIDADO_ID")
    private Integer detalleConsolidadoId;
    @Size(max = 128)
    @Column(name = "ITEMS")
    private String items;
    @Column(name = "CANTIDAD")
    private Integer cantidad;
    @Size(max = 264)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Size(max = 4)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "MC_CONSOLIDADO_ID", referencedColumnName = "CONSOLIDADO_ID")
    @ManyToOne
    private McConsolidado mcConsolidadoId;

    public McDetalleConsolidado() {
    }

    public McDetalleConsolidado(Integer detalleConsolidadoId) {
        this.detalleConsolidadoId = detalleConsolidadoId;
    }

    public Integer getDetalleConsolidadoId() {
        return detalleConsolidadoId;
    }

    public void setDetalleConsolidadoId(Integer detalleConsolidadoId) {
        this.detalleConsolidadoId = detalleConsolidadoId;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public McConsolidado getMcConsolidadoId() {
        return mcConsolidadoId;
    }

    public void setMcConsolidadoId(McConsolidado mcConsolidadoId) {
        this.mcConsolidadoId = mcConsolidadoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detalleConsolidadoId != null ? detalleConsolidadoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McDetalleConsolidado)) {
            return false;
        }
        McDetalleConsolidado other = (McDetalleConsolidado) object;
        if ((this.detalleConsolidadoId == null && other.detalleConsolidadoId != null) || (this.detalleConsolidadoId != null && !this.detalleConsolidadoId.equals(other.detalleConsolidadoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sofya.java.mcbestpc.backend.model.McDetalleConsolidado[ detalleConsolidadoId=" + detalleConsolidadoId + " ]";
    }
    
}
