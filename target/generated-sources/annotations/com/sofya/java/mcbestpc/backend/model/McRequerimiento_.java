package com.sofya.java.mcbestpc.backend.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McRequerimiento.class)
public abstract class McRequerimiento_ {

	public static volatile SingularAttribute<McRequerimiento, Date> fecha;
	public static volatile SingularAttribute<McRequerimiento, String> estado;
	public static volatile SingularAttribute<McRequerimiento, Integer> requerimientoId;
	public static volatile ListAttribute<McRequerimiento, McRequerimientoConsolidado> mcRequerimientoConsolidadoList;
	public static volatile ListAttribute<McRequerimiento, McDetalleRequerimientos> mcDetalleRequerimientosList;
	public static volatile SingularAttribute<McRequerimiento, McAgencia> mcAgenciaId;

}

