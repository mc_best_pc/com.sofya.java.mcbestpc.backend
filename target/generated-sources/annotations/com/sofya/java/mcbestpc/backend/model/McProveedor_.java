package com.sofya.java.mcbestpc.backend.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McProveedor.class)
public abstract class McProveedor_ {

	public static volatile SingularAttribute<McProveedor, String> estado;
	public static volatile SingularAttribute<McProveedor, Integer> proveedorId;
	public static volatile ListAttribute<McProveedor, McCotizacion> mcCotizacionList;

}

