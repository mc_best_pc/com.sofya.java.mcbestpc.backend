package com.sofya.java.mcbestpc.backend.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McRequerimientoConsolidado.class)
public abstract class McRequerimientoConsolidado_ {

	public static volatile SingularAttribute<McRequerimientoConsolidado, McConsolidado> mcConsolidadoId;
	public static volatile SingularAttribute<McRequerimientoConsolidado, String> estado;
	public static volatile SingularAttribute<McRequerimientoConsolidado, McRequerimiento> mcRequerimientoId;
	public static volatile SingularAttribute<McRequerimientoConsolidado, Date> fechaAprobacion;
	public static volatile SingularAttribute<McRequerimientoConsolidado, Integer> requerimientoConsolidadoId;
	public static volatile SingularAttribute<McRequerimientoConsolidado, String> usuario;
	public static volatile ListAttribute<McRequerimientoConsolidado, McReqConsCotizacion> mcReqConsCotizacionList;

}

