package com.sofya.java.mcbestpc.backend.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McDetalleCotizacion.class)
public abstract class McDetalleCotizacion_ {

	public static volatile SingularAttribute<McDetalleCotizacion, String> descripcion;
	public static volatile SingularAttribute<McDetalleCotizacion, String> estado;
	public static volatile SingularAttribute<McDetalleCotizacion, Integer> detalleCotizacionId;
	public static volatile SingularAttribute<McDetalleCotizacion, McCotizacion> idCotizacion;
	public static volatile SingularAttribute<McDetalleCotizacion, Integer> cantidad;
	public static volatile SingularAttribute<McDetalleCotizacion, String> items;

}

