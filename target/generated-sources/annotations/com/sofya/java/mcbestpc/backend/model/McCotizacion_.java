package com.sofya.java.mcbestpc.backend.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McCotizacion.class)
public abstract class McCotizacion_ {

	public static volatile SingularAttribute<McCotizacion, Date> fecha;
	public static volatile SingularAttribute<McCotizacion, String> estado;
	public static volatile SingularAttribute<McCotizacion, McProveedor> proveedorId;
	public static volatile SingularAttribute<McCotizacion, Integer> idCotizacion;
	public static volatile ListAttribute<McCotizacion, McReqConsCotizacion> mcReqConsCotizacionList;
	public static volatile ListAttribute<McCotizacion, McDetalleCotizacion> mcDetalleCotizacionList;

}

