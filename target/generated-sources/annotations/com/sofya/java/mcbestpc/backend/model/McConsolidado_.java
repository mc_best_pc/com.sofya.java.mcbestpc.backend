package com.sofya.java.mcbestpc.backend.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McConsolidado.class)
public abstract class McConsolidado_ {

	public static volatile SingularAttribute<McConsolidado, String> estado;
	public static volatile SingularAttribute<McConsolidado, Date> fechaConsolidado;
	public static volatile SingularAttribute<McConsolidado, Integer> consolidadoId;
	public static volatile ListAttribute<McConsolidado, McRequerimientoConsolidado> mcRequerimientoConsolidadoList;
	public static volatile ListAttribute<McConsolidado, McDetalleConsolidado> mcDetalleConsolidadoList;

}

