package com.sofya.java.mcbestpc.backend.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McReqConsCotizacion.class)
public abstract class McReqConsCotizacion_ {

	public static volatile SingularAttribute<McReqConsCotizacion, Date> fecha;
	public static volatile SingularAttribute<McReqConsCotizacion, String> estado;
	public static volatile SingularAttribute<McReqConsCotizacion, Integer> reqConsCotizacion;
	public static volatile SingularAttribute<McReqConsCotizacion, McCotizacion> idCotizacion;
	public static volatile SingularAttribute<McReqConsCotizacion, McRequerimientoConsolidado> requerimientoConsolidadoId;

}

