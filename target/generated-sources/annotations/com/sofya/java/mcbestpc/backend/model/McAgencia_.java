package com.sofya.java.mcbestpc.backend.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McAgencia.class)
public abstract class McAgencia_ {

	public static volatile SingularAttribute<McAgencia, Integer> agenciaId;
	public static volatile SingularAttribute<McAgencia, String> estado;
	public static volatile ListAttribute<McAgencia, McRequerimiento> mcRequerimientoList;
	public static volatile SingularAttribute<McAgencia, String> direccion;
	public static volatile SingularAttribute<McAgencia, String> nombre;
	public static volatile SingularAttribute<McAgencia, String> puntoEmision;

}

