package com.sofya.java.mcbestpc.backend.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McDetalleConsolidado.class)
public abstract class McDetalleConsolidado_ {

	public static volatile SingularAttribute<McDetalleConsolidado, String> descripcion;
	public static volatile SingularAttribute<McDetalleConsolidado, McConsolidado> mcConsolidadoId;
	public static volatile SingularAttribute<McDetalleConsolidado, String> estado;
	public static volatile SingularAttribute<McDetalleConsolidado, Integer> cantidad;
	public static volatile SingularAttribute<McDetalleConsolidado, Integer> detalleConsolidadoId;
	public static volatile SingularAttribute<McDetalleConsolidado, String> items;

}

