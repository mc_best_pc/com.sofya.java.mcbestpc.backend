package com.sofya.java.mcbestpc.backend.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(McDetalleRequerimientos.class)
public abstract class McDetalleRequerimientos_ {

	public static volatile SingularAttribute<McDetalleRequerimientos, String> descripcion;
	public static volatile SingularAttribute<McDetalleRequerimientos, String> estado;
	public static volatile SingularAttribute<McDetalleRequerimientos, McRequerimiento> mcRequerimientoId;
	public static volatile SingularAttribute<McDetalleRequerimientos, Integer> cantidad;
	public static volatile SingularAttribute<McDetalleRequerimientos, String> items;
	public static volatile SingularAttribute<McDetalleRequerimientos, Integer> detalleRequerimientoId;

}

